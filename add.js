const socket = new WebSocket('ws://192.168.100.116:5678');
let lastPostive = [];
let pointsList = [];
let canvas1data = null;
let imageObj = null;
// let drawingState = false;
// let pastState = [0, 0];
let photo = null;
let url = null;
let redClicked = false;
let greenClicked = true;
let canvas = new fabric.Canvas('leftcanvas',{
    height:650,
    width:650,
    hoverCursor: "pointer"
});

let canvas2 = document.getElementById("rightcanvas");
canvas2.width = 650;
canvas2.height = 650;

// let canvas2 = new fabric.Canvas('rightcanvas',{
    
//     height:650,
//     width:650 
// });
let canvasClear = (can) => {
    can.clear();
    can.set({
        height:650,
        width:650
    })
}
socket.addEventListener('open', function (event) {
    console.log("Connection is open");
});
socket.addEventListener('close', function (event) {
    console.log('Connection is closed');
});
socket.addEventListener('message', function (event) {
    let msg = event.data;
    let message = JSON.parse(msg);
    let _msg = message["data"];
    
    console.log('Message from server ', _msg['img']);
    canv=document.createElement("canvas");
    canv.height = 650;
    canv.width = 650;
    context = canv.getContext("2d");
    // fabric.Image.fromURL('data:image/jpeg;base64,'+_msg['img'], function(oImg) {
    //     resizeImg(oImg);
    //     canvas2.add(oImg);
    //     });
    let ctx = canv.getContext("2d");
    let image = new Image();
    image.onload = function() {
        // image = resizeDummyImg(image);
        ctx.drawImage(image, 0, 0, imageObj.getScaledWidth(),imageObj.getScaledHeight());
        // console.log(ctx.getImageData(0,0,100,100));
        console.log(typeof(imageObj.getScaledHeight().toString()))
        const arr = Array.prototype.slice.call(ctx.getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight())["data"]);
        // canvas1data = canvas2.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight());
        // console.log(canvas.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight()));
        let i;
        console.log(canvas1data.data.length);
        console.log(arr.length);
        for (i = 0; i<(arr.length/4); i++){
            // console.log("before", canvas1data.data[i*4+3])
            canvas1data.data[(i*4)+3] = arr[(i*4)+1];
            // console.log("after", canvas1data.data[i*4+3])
        }
        console.log(arr);
        canvas2.getContext("2d").putImageData(canvas1data,0,0);
        // const uniqueArr = [...new Set(arr)];
        // console.log(uniqueArr);
        // const arrs = ctx.getImageData(10,10,100,100);
        // const arrs = ctx.getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight());
        // const arr = Array.prototype.slice.call(arrs["data"]);
        // const uniqueArr = [...new Set(arr)];
        // console.log(arrs);
    };
    image.src = 'data:image/jpeg;base64,'+_msg['img'];
    
    
});
let imageUpload = function () {
    photo = this.files[0];
    console.log(photo);
    const file = photo;
    const reader = new FileReader();
    reader.addEventListener("load", function () {
        // console.log(reader.result)
        url = reader.result;
        
        fabric.Image.fromURL(url, function(oImg) {
            resizeImg(oImg)
            
            // oImg.filters.push(new fabric.Image.filters.Resize({
            //     resizeType: 'sliceHack',
            //     scaleX: 0.5,
            //     scaleY: 0.5
            // }));
            // oImg.applyFilters();
          
          
            // canvasClear(canvas2);
            canvasClear(canvas);
            canvas.add(oImg);
            url = imageObj.toDataURL()
            
            // Generate the image file
            // var image = Canvas2Image.saveAsPNG(canvas, true);   

            // image.id = "canvasimage";
            // canvas.parentNode.replaceChild(image, canvas);

            var _URL = 'upload1.php';
            // data = $('#canvasimage').attr('src');

            $.ajax({ 
                type: "POST", 
                url: _URL,
                dataType: 'text',
                data: {
                    base64data : url
                }
            });
            canvas2.getContext("2d").clearRect(0, 0, canvas2.width, canvas2.height);
            let img = new Image();
            img.onload = () => {
                canvas2.getContext("2d").drawImage(img,0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight());
                canvas1data = canvas2.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight())
                
                if(socket.readyState === WebSocket.OPEN){
                    console.log(canvas2.toDataURL())
                    msg = {"event": "image", "data": canvas2.toDataURL()}
                    socket.send(JSON.stringify(msg))
                }
            } 
            img.src = url;
            // canvas.getContext("2d").getImageData(0,)
            // console.log(canvas.toDataURL('image/png'))
            // canvas2.add(oImg);
            // data = canvas.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight())
            // canvas2.add(oImg);
            // canvas2 = document.getElementById('rightcanvas');
            // canvas2.height = 650;
            // canvas2.width = 650;
            // canvas2.getContext("2d").putImageData(data, 0, 0)
            // let image = new Image();
            // image.onload = function() {
            //     ctx.drawImage(image, 0, 0);
            // }
            // image.src = url
            });
    }, false);
    if (file) {
        reader.readAsDataURL(file);
    }
}
let addgreen = () => {
    redClicked = false;
    greenClicked = true;
}
let addred = () => {
    redClicked = true;
    greenClicked = false;
}
let undo = () => {
    if(lastPostive.length === 1){
        // canvasClear(canvas2);
        canvas2.getContext("2d").clearRect(0, 0, canvas2.width, canvas2.height);
        let img = new Image();
        img.onload = () => {
            canvas2.getContext("2d").drawImage(img,0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight());
            // canvas1data = canvas2.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight())
        } 
        img.src = url;
    }
    
    let isPositive = lastPostive.pop();
    // console.log(isPositive);
    canvas.remove(pointsList.pop());
    socket.send(JSON.stringify({"event": "UNDO",
                                "data": isPositive}));
    if(lastPostive.length !== 0){
        socket.send(JSON.stringify({"event": "register"}));
    }
}
let clear = () => {
    lastPostive = [];
    pointsList.forEach(points => {
        canvas.remove(points);
    });
    canvas2.getContext("2d").clearRect(0, 0, canvas2.width, canvas2.height);
    let img = new Image();
    img.onload = () => {
        canvas2.getContext("2d").drawImage(img,0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight());
        // canvas1data = canvas2.getContext("2d").getImageData(0,0,imageObj.getScaledWidth(),imageObj.getScaledHeight())
    } 
    img.src = url;
    socket.send(JSON.stringify({"event": "CLEAR"}));

}
let resizeImg = (img) => {
    let scaleElementX = 1;
    let scaleElementY = 1;
    if(img.width > img.height){
        scaleElementY = canvas.width/img.width;
        img.set({
            scaleY:scaleElementY,
            scaleX:scaleElementY
        });
    }
    else{
        scaleElementX = canvas.height/img.height;
        img.set({
            scaleY:scaleElementX,
            scaleX:scaleElementX
        });
    }
}

let onObjectAdded  = (event) => {
    const addedObject = event.target;
    
    if (addedObject.type === "circle"){
        addedObject.set({
            "selectable": false
        });
        pointsList.push(addedObject)
    }
    else if(addedObject.type === "image"){
        // canvasClear(canvas2);
        imageObj = addedObject;
        addedObject.set({
            "selectable": false
        }); 
    }
}
let onObjectAdded2  = (event) => {
    const addedObject = event.target;
    
    if(addedObject.type === "image"){
        addedObject.set({
            "selectable": false
        }); 
        
        
    }
    
}

let mouseDown = (opt) => {
       
    // pastState = [Math.round(opt.e.clientX/imageObj.scaleX), Math.round(opt.e.clientY/imageObj.scaleY)];
    console.log(opt.e.clientX);
    console.log(opt);
    if (greenClicked){ 
  
        console.log("greeen clicked");
        let circle = new fabric.Circle({
            fill: "#00ff00",
            originX: 'center',
            originY: 'center',
            radius: 3,
            left:opt.pointer.x,
            top:opt.pointer.y
        });
        canvas.add(circle);
        // msg = {"event": "f", "data": {"x": (opt.pointer.x)/imageObj.scaleX, "y": opt.pointer.y/imageObj.scaleY}};
        msg = {"event": "f", "data": {"x": opt.pointer.x, "y": opt.pointer.y}};
        socket.send(JSON.stringify(msg));
        socket.send(JSON.stringify({"event": "register"}));
    }
    else if (redClicked){
        lastPostive.push(false);
        console.log("red clicked");
        let circle = new fabric.Circle({
            fill: "#ff0000",
            originX: 'center',
            originY: 'center',
            radius: 3,
            left:opt.pointer.x,
            top:opt.pointer.y
        });
        canvas.add(circle);
        // let circle1 = new fabric.Circle({
        //     fill: "#ff0000",
        //     originX: e.clientX,
        //     originY: e.,
        //     radius: 3,
            
        // });
        // canvas.add(circle1);
        // msg = {"event": "b", "data": {"x": Math.round(opt.pointer.x/imageObj.scaleX), "y": Math.round(opt.pointer.y/imageObj.scaleY)}};
        msg = {"event": "b", "data": {"x": opt.pointer.x, "y": opt.pointer.y}};
        socket.send(JSON.stringify(msg));
        socket.send(JSON.stringify({"event": "register"}));
    }    
}

canvas.on('mouse:down', mouseDown);
canvas.on('object:added', onObjectAdded);
// canvas2.on('object:added', onObjectAdded2);
$('#imgUploaded').change(imageUpload);
$("#plusbtn").click(addgreen);
$("#minusbtn").click(addred);
$('#undo').click(undo);
$('#clearAll').click(clear);


